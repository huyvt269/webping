import json
import socket

import dns
from django.core.exceptions import ValidationError


def get_ip_address(domain):
    ip_address = None
    try:
        ip_address = socket.gethostbyname(domain)
    except Exception:
        raise ValidationError("Không tìm thấy địa chỉ IP")
    return ip_address


def get_name_server(domain):
    name_servers = None
    try:
        name_servers = json.dumps([str(x) for x in dns.resolver.query(domain, 'NS')])
    except Exception:
        raise ValidationError("Không tìm thấy máy chủ DNS")
    return name_servers


def get_website_object(website_model, data, user):
    website_model.id = data['id']
    website_model.name = data['name']
    website_model.uri = data['uri']
    website_model.gaptime_monitor = data['gaptime_monitor']
    website_model.status_monitor = data['status_monitor']
    website_model.create_at = data['create_at']
    website_model.update_at = data['update_at']
    website_model.name_server = data['name_server']
    website_model.ip_address = data['ip_address']
    website_model.user_id = user
    return website_model
