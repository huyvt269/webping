from django.contrib.auth import views as auth_views
from django.urls import path

from app.forms import EmailValidationOnForgotPassword
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('register/', views.register, name='register'),
    path('landing_page', views.landing_page, name='landing_page'),
    path('password_reset/', auth_views.PasswordResetView.as_view(
        template_name='pages/password_reset.html',
        form_class=EmailValidationOnForgotPassword,
        email_template_name='pages/password_reset_email.html',
        subject_template_name='pages/password_reset_subject.html'),
         name='password_reset'),
    path('password_reset/done', auth_views.PasswordResetDoneView.as_view(
        template_name='pages/password_reset_done.html'),
         name='password_reset_done'),
    path('password_reset_confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='pages/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password_reset_complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='pages/password_reset_complete.html'),
         name='password_reset_complete'),
    path('profile/<int:user_id>/', views.profile, name='profile'),
    path('change_password/', views.change_password, name='change_password'),
    path('websites/', views.retrieve_websites, name='websites'),
    path('websites/add', views.add_website, name='add_website'),
    path('websites/<uuid:id>', views.monitor_website, name='monitor_website'),
    path('websites/<uuid:id>/edit', views.edit_website, name='edit_website'),
    path('websites/<uuid:id>/delete', views.delete_website, name='delete_website'),

]
