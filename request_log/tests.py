import mock
from django.contrib.auth import get_user_model
from django.http import HttpResponse, HttpRequest
from django.test import TestCase, RequestFactory

from .middleware import RequestMiddleware
from .models import Request


class RequestMiddlewareTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = RequestMiddleware()

    def test_record(self):
        request = self.factory.get('/foo')
        response = HttpResponse()
        self.middleware.process_response(request, response)
        self.assertEqual(1, Request.objects.count())

    @mock.patch('django.conf.settings.MIDDLEWARE', [
        'django.contrib.sessions.middleware.SessionMiddleware',
        'request_log.middleware.RequestMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
    ])
    def test_middleware_functions_supported(self):
        request = self.factory.get('/foo')
        RequestMiddleware(request)

    @mock.patch('request_log.settings.VALID_METHOD_NAMES',
                ('get',))
    def test_dont_record_unvalid_method_name(self):
        request = self.factory.post('/foo')
        response = HttpResponse()
        self.middleware.process_response(request, response)
        self.assertEqual(0, Request.objects.count())

    @mock.patch('request_log.middleware.settings.VALID_METHOD_NAMES',
                ('get',))
    def test_record_valid_method_name(self):
        request = self.factory.get('/foo')
        response = HttpResponse()
        self.middleware.process_response(request, response)
        self.assertEqual(1, Request.objects.count())

    @mock.patch('request_log.middleware.settings.ONLY_ERRORS',
                False)
    def test_dont_record_only_error(self):
        request = self.factory.get('/foo')
        # Errored
        response = HttpResponse(status=500)
        self.middleware.process_response(request, response)
        # Succeed
        response = HttpResponse(status=200)
        self.middleware.process_response(request, response)

        self.assertEqual(2, Request.objects.count())

    @mock.patch('request_log.middleware.settings.ONLY_ERRORS',
                True)
    def test_record_only_error(self):
        request = self.factory.get('/foo')
        # Errored
        response = HttpResponse(status=500)
        self.middleware.process_response(request, response)
        # Succeed
        response = HttpResponse(status=200)
        self.middleware.process_response(request, response)

        self.assertEqual(1, Request.objects.count())

    @mock.patch('request_log.middleware.settings.IGNORE_PATHS',
                (r'^foo',))
    def test_dont_record_ignored_paths(self):
        request = self.factory.get('/foo')
        response = HttpResponse()
        # Ignored path
        self.middleware.process_response(request, response)
        # Recorded
        request = self.factory.get('/bar')
        self.middleware.process_response(request, response)

        self.assertEqual(1, Request.objects.count())

    @mock.patch('request_log.middleware.settings.IGNORE_IP',
                ('1.2.3.4',))
    def test_dont_record_ignored_ips(self):
        request = self.factory.get('/foo')
        response = HttpResponse()
        # Ignored IP
        request.META['REMOTE_ADDR'] = '1.2.3.4'
        self.middleware.process_response(request, response)
        # Recorded
        request.META['REMOTE_ADDR'] = '5.6.7.8'
        self.middleware.process_response(request, response)

        self.assertEqual(2, Request.objects.count())

    @mock.patch('request_log.middleware.settings.IGNORE_USER_AGENTS',
                (r'^.*Foo.*$',))
    def test_dont_record_ignored_user_agents(self):
        request = self.factory.get('/foo')
        response = HttpResponse()
        # Ignored
        request.META['HTTP_USER_AGENT'] = 'Foo'
        self.middleware.process_response(request, response)
        request.META['HTTP_USER_AGENT'] = 'FooV2'
        self.middleware.process_response(request, response)
        # Recorded
        request.META['HTTP_USER_AGENT'] = 'Bar'
        self.middleware.process_response(request, response)
        request.META['HTTP_USER_AGENT'] = 'BarV2'
        self.middleware.process_response(request, response)

        self.assertEqual(2, Request.objects.count())

    @mock.patch('request_log.middleware.settings.IGNORE_USERNAME',
                ('foo',))
    def test_dont_record_ignored_user_names(self):
        request = self.factory.get('/foo')
        response = HttpResponse()
        # Anonymous
        self.middleware.process_response(request, response)
        # Ignored
        request.user = get_user_model().objects.create(username='foo')
        self.middleware.process_response(request, response)
        # Recorded
        request.user = get_user_model().objects.create(username='bar')
        self.middleware.process_response(request, response)

        self.assertEqual(2, Request.objects.count())


class RequestTest(TestCase):
    def test_from_http_request(self):
        http_request = HttpRequest()
        http_request.method = 'PATCH'
        http_request.path = '/kylef'

        http_response = HttpResponse(status=204)

        request = Request()
        request.from_http_request(http_request, http_response, commit=False)

        self.assertEqual(request.path, '/kylef')
        self.assertEqual(request.method, 'PATCH')
        self.assertEqual(request.response, 204)

    def test_from_http_request_with_user(self):
        http_request = HttpRequest()
        http_request.method = 'GET'
        http_request.user = get_user_model().objects.create(username='foo')

        request = Request()
        request.from_http_request(http_request, commit=False)
        self.assertEqual(request.user.id, http_request.user.id)

    def test_from_http_request_redirection(self):
        http_request = HttpRequest()
        http_request.method = 'GET'
        http_response = HttpResponse(status=301)
        http_response['Location'] = '/foo'

        request = Request()
        request.from_http_request(http_request, http_response, commit=False)
        self.assertEqual(request.redirect, '/foo')

    def test_from_http_request_not_commit(self):
        http_request = HttpRequest()
        http_request.method = 'GET'

        request = Request()
        request.from_http_request(http_request, commit=False)
        self.assertIsNone(request.id)
