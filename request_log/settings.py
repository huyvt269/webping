from django.conf import settings

VALID_METHOD_NAMES = getattr(
    settings,
    'REQUEST_VALID_METHOD_NAMES',
    ('get', 'post', 'put', 'delete', 'head', 'options', 'trace'),
)

ONLY_ERRORS = getattr(settings, 'REQUEST_ONLY_ERRORS', False)
IGNORE_IP = getattr(settings, 'REQUEST_IGNORE_IP', tuple())
IGNORE_USERNAME = getattr(settings, 'REQUEST_IGNORE_USERNAME', tuple())
IGNORE_PATHS = getattr(settings, 'REQUEST_IGNORE_PATHS', tuple())
IGNORE_USER_AGENTS = getattr(settings, 'REQUEST_IGNORE_USER_AGENTS', tuple())

try:
    from django.http import HttpRequest
    from django.contrib.sites.shortcuts import get_current_site

    BASE_URL = getattr(settings, 'REQUEST_BASE_URL', 'http://{0}'.format(get_current_site(HttpRequest()).domain))
except Exception:
    BASE_URL = getattr(settings, 'REQUEST_BASE_URL', 'http://127.0.0.1')
